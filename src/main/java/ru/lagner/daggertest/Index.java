package ru.lagner.daggertest;


class Index
{
    static public void main(String[] args)
    {
        AppComponent app = DaggerAppComponent.builder()
            .build();
            // .appModule(new AppModule())

        System.out.println("appComponent was built");

        IndexActivity myActivity = new IndexActivity(null);
        System.out.println("indexActivity was created");
        
        try { 
            IndexActivity activity = app.indexActivity();
            activity.run();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

package ru.lagner.daggertest;

import dagger.Module;
import dagger.Provides;


@Module
class AppModule {

    @Provides
    public static IAuthService provideIAuthService() {
        return new FirebaseAuthService();
    }
}


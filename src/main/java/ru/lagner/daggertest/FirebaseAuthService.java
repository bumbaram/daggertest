package ru.lagner.daggertest;


class FirebaseAuthService implements IAuthService {

    private boolean mIsAuthorized = false;

    @Override
    public boolean isAuthorized() {
        return mIsAuthorized;
    }
}

package ru.lagner.daggertest;


public interface IAuthService {

    public boolean isAuthorized();

}

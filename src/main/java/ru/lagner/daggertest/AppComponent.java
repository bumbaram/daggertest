package ru.lagner.daggertest;

import dagger.Component;


@Component(modules = AppModule.class)
interface AppComponent {

    IndexActivity indexActivity();
}

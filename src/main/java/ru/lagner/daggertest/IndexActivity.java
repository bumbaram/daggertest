package ru.lagner.daggertest;

import javax.inject.Inject;


class IndexActivity {

    private final IAuthService mAuth;

    @Inject
    public IndexActivity(IAuthService auth) {
        mAuth = auth;
    }

    public void run() {

        if (mAuth == null) {
            System.out.println("Auth is null. Do nothing");
            return;
        }

        if (mAuth.isAuthorized()) {
            System.out.println("user is authorized go to main activity");

        } else {
            System.out.println("Not Authorized. Go to Login");
        }
    }
}
